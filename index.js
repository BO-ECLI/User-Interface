/* eslint-disable */

// App entry point

if (process.env.NODE_ENV === 'production') {
  // In production require the server webpacked file.
  require('./dist/server.bundle.js');
} else {
  // Development environment
  // Convert ES6 code at runtime
  require('babel-register')();
  require('./server/server');
}
