import React, { Component } from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import CircularProgress from 'material-ui/CircularProgress';
import { Tabs, Tab } from 'material-ui/Tabs';
import spacing from 'material-ui/styles/spacing';

import JsonView from './JsonView';

const desktopGutter = spacing.desktopGutter;

const cardStyle = {
  marginTop: desktopGutter,
  marginBottom: 100,
};

class ProposalResultBlock extends Component {
  render() {
    const references = this.props.references;
    const referencesCount = references.length;
    const progress = (<CardText style={{ textAlign: 'center' }}>
      <CircularProgress size={80} thickness={5} />
    </CardText>);
    const tabs = (
      <Tabs>
        <Tab label="JSON view" >
          <JsonView data={references} />
        </Tab>
      </Tabs>
    );

    return (
      <Card style={cardStyle}>
        <CardTitle
          title="Result"
          subtitle={!this.props.loading ? `References: ${referencesCount}` : ''}
        />
        { this.props.loading && progress }
        {!this.props.loading && referencesCount && (<CardText>{tabs}</CardText>)}
      </Card>
    );
  }
}

export default ProposalResultBlock;
