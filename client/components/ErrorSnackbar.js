import React from 'react';
import Snackbar from 'material-ui/Snackbar';

const ErrorSnackbar = ({ message, onRequestClose }) => (
  <Snackbar
    open
    bodyStyle={{ backgroundColor: 'rgba(220, 10, 0, 0.6)' }}
    autoHideDuration={3000}
    message={message}
    onRequestClose={onRequestClose}
  />
);

export default ErrorSnackbar;
