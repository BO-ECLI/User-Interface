import React from 'react';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import SplitIcon from 'material-ui/svg-icons/communication/call-split';

import { Toolbar, ToolbarGroup, ToolbarSeparator } from 'material-ui/Toolbar';

const EUlogo = require('../img/EUflag.png');
const koopLogo = require('../img/koop-logo.png');
const ittigLogo = require('../img/LogoITTIG.png');
const cirsfidLogo = require('../img/logo-cirsfid-small.jpg');
const unitoLogo = require('../img/Logo_Unito.png');
const cendojLogo = require('../img/cendoj-logo.png');

const styles = {
  toolbar: {
    position: 'absolute',
    width: '100%',
    right: 0,
    bottom: 0,
    left: 0,
    height: 90,
    marginTop: 20,
  },
  toolbarFork: {
    position: 'absolute',
    width: '100%',
    right: 0,
    bottom: 90,
    left: 0,
    height: 30,
  },
  forkButton: {
    margin: '0 auto',
  },
  euIcon: {
    width: 63,
    height: 42,
  },
  euIconBtn: {
    width: 83,
    height: 62,
  },
  koopIcon: {
    width: 144,
    height: 60,
  },
  koopBtn: {
    width: 144,
    height: 60,
    marginLeft: 5,
    marginRight: 5,
    padding: 0,
  },
  unitoIcon: {
    width: 154,
    height: 60,
  },
  unitoBtn: {
    width: 154,
    height: 60,
    marginLeft: 5,
    marginRight: 5,
    padding: 0,
  },
  ittigIcon: {
    width: 67,
    height: 60,
  },
  ittigBtn: {
    width: 67,
    height: 60,
    marginLeft: 5,
    marginRight: 5,
    padding: 0,
  },
  cirsfidIcon: {
    width: 90,
    height: 60,
  },
  cirsfidBtn: {
    width: 90,
    height: 60,
    marginLeft: 5,
    marginRight: 5,
    padding: 0,
  },
  cendojIcon: {
    width: 98,
    height: 60,
  },
  cendojBtn: {
    width: 98,
    height: 60,
    marginLeft: 5,
    marginRight: 5,
    padding: 0,
  },
  disclaimer: {
    fontSize: '0.8em',
    color: 'rgba(0, 0, 0, 0.60)',
  },
};

const Footer = () =>
  <div>
    <Toolbar
      style={styles.toolbar}
    >
      <ToolbarGroup
        firstChild
      >
        <IconButton
          iconStyle={styles.euIcon}
          style={styles.euIconBtn}
        >
          <img src={EUlogo} alt="EU logo" />
        </IconButton>
      </ToolbarGroup>
      <ToolbarGroup>
        <p style={styles.disclaimer}>
          This website has been produced with the financial support of the Justice Programme of the European Union. The contents of this website are the sole responsibility of the partners of the BO-ECLI project and can in no way be taken to reflect the views of the European Commission.
        </p>
      </ToolbarGroup>
      <ToolbarGroup>
        <ToolbarSeparator />
      </ToolbarGroup>
      <ToolbarGroup>
        <IconButton
          iconStyle={styles.ittigIcon}
          style={styles.ittigBtn}
          tooltip="ITTIG"
          href="http://www.ittig.cnr.it/"
        >
          <img src={ittigLogo} alt="ITTIG logo" />
        </IconButton>
        <IconButton
          iconStyle={styles.cirsfidIcon}
          style={styles.cirsfidBtn}
          tooltip="CIRSFID, Unibo"
          href="http://www.cirsfid.unibo.it/"
        >
          <img src={cirsfidLogo} alt="CIRSFID logo" />
        </IconButton>
        <IconButton
          iconStyle={styles.koopIcon}
          style={styles.koopBtn}
          tooltip="KOOP"
          href="http://koop.overheid.nl/"
        >
          <img src={koopLogo} alt="KOOP logo" />
        </IconButton>
        <IconButton
          iconStyle={styles.unitoIcon}
          style={styles.unitoBtn}
          tooltip="UNITO"
          href="https://www.unito.it/"
        >
          <img src={unitoLogo} alt="UNITO logo" />
        </IconButton>
        <IconButton
          iconStyle={styles.cendojIcon}
          style={styles.cendojBtn}
          tooltip="CENDOJ"
          href="http://www.poderjudicial.es/search/"
        >
          <img src={cendojLogo} alt="CENDOJ logo" />
        </IconButton>
      </ToolbarGroup>
    </Toolbar>
    <Toolbar style={styles.toolbarFork} >
      <FlatButton
        href="https://gitlab.com/BO-ECLI"
        target="_blank"
        label="Fork me on GitLab"
        icon={<SplitIcon />}
        style={styles.forkButton}
      />
    </Toolbar>
  </div>;

export default Footer;
