import React, { Component } from 'react';
import { Card, CardActions, CardText } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ActionSettings from 'material-ui/svg-icons/action/settings';
import ContentSend from 'material-ui/svg-icons/content/send';
import UnarchiveIcon from 'material-ui/svg-icons/content/unarchive';
import RefreshIcon from 'material-ui/svg-icons/navigation/refresh';
import spacing from 'material-ui/styles/spacing';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Toggle from 'material-ui/Toggle';

import { sendParseRequest, sendCompelRequest } from '../actions/index';

import ExampleBlock from './ExampleBlock';

const minInputTextLength = 3;
const desktopGutter = spacing.desktopGutter;

const inputTextHintStyle = {
  bottom: '50%',
};

const floatingButtonStyle = {
  position: 'fixed',
  bottom: desktopGutter * 4,
  right: desktopGutter,
  zIndex: 1,
};

// Some european languages
const availableLangs = ['DE', 'ES', 'FR', 'IT', 'NL', 'RO'];

class InputBlock extends Component {
  constructor(props) {
    super(props);
    this.initialState = {
      text: '',
      language: 'IT',
      jurisdiction: 'IT',
      inputEcli: '',
      enableHudoc: false,
      enableCellar: false,
      languages: availableLangs,
      jurisdictions: availableLangs,
      showOptions: false,
      showExamples: false,
    };
    this.state = this.initialState;

    this.sendRequest = this.sendRequest.bind(this);
    this.sendCompelRequest = this.sendCompelRequest.bind(this);
    this.sendParseRequest = this.sendParseRequest.bind(this);
    this.loadExample = this.loadExample.bind(this);
    this.setInitialState = this.setInitialState.bind(this);

    // getLanguages().then(({ data: languages }) =>
    //   this.setState({ languages, language: languages[0] })
    // );
    // getJurisdictions().then(({ data: jurisdictions }) =>
    //   this.setState({ jurisdictions, jurisdiction: jurisdictions[0] })
    // );
  }

  setInitialState() {
    this.setState(this.initialState);
  }

  sendRequest() {
    const { text } = this.state;
    try {
      this.sendCompelRequest(JSON.parse(text));
    } catch (e) {
      this.sendParseRequest();
    }
  }

  sendCompelRequest(json) {
    const onLoadingChange = this.props.onLoadingChange;
    const onReferencesChange = this.props.onReferencesChange;
    const onRequestError = this.props.onRequestError;
    onLoadingChange(true);
    sendCompelRequest({ json })
      .then(({ data }) => {
        onLoadingChange(false);
        onReferencesChange(data);
      })
      .catch(() => onRequestError('There was an error in the request.'));
  }

  sendParseRequest() {
    const { text,
             language,
             jurisdiction,
             inputEcli,
             enableHudoc,
             enableCellar,
            } = this.state;
    const onLoadingChange = this.props.onLoadingChange;
    const onReferencesChange = this.props.onReferencesChange;
    const onRequestError = this.props.onRequestError;
    onLoadingChange(true);
    sendParseRequest({
      text,
      language,
      jurisdiction,
      'input-ecli': inputEcli,
      enableHudoc,
      enableCellar,
    })
      .then(({ data: references }) => {
        onLoadingChange(false);
        onReferencesChange(references);
      })
      .catch(() => onRequestError('There was an error in the request.'));
  }

  loadExample(data) {
    const { language, inputEcli, text } = data;
    this.setState({ language, inputEcli, text, jurisdiction: language });
  }

  render() {
    const getLanguageMenuItems = () =>
      this.state.languages.map(language =>
        <MenuItem value={language} key={language} primaryText={language} />
      );
    const getJurisdictionMenuItems = () =>
      this.state.jurisdictions.map(jurisdiction =>
        <MenuItem value={jurisdiction} key={jurisdiction} primaryText={jurisdiction} />
      );
    const advancedOptions = (
      <div>
        <SelectField
          floatingLabelText="Jurisdiction"
          hintText="Select a jurisdiction"
          value={this.state.jurisdiction}
          onChange={(evt, i, jurisdiction) => this.setState({ jurisdiction })}
        >
          {getJurisdictionMenuItems()}
        </SelectField>
        <br />
        <TextField
          floatingLabelText="Input ECLI"
          hintText="Insert input ECLI"
          value={this.state.inputEcli}
          onChange={evt => this.setState({ inputEcli: evt.target.value })}
        />
        <br />
        <Toggle
          label="Enable access to Hudoc"
          labelPosition="right"
          value={this.state.enableHudoc}
          onToggle={(evt, enableHudoc) => this.setState({ enableHudoc })}
        />
        <br />
        <Toggle
          label="Enable access to Cellar"
          labelPosition="right"
          value={this.state.enableCellar}
          onToggle={(evt, enableCellar) => this.setState({ enableCellar })}
        />
      </div>);

    return (
      <div>
        <Card>
          <CardText>
            <TextField
              hintText="Copy here your text or the ECLI from Judiciary website."
              multiLine
              fullWidth
              rows={5}
              rowsMax={15}
              hintStyle={inputTextHintStyle}
              value={this.state.text}
              onChange={evt => this.setState({ text: evt.target.value })}
            />
            <div style={{ display: 'flex' }}>
              <div style={{ flex: 1 }}>
                <SelectField
                  floatingLabelText="Language"
                  hintText="Select a language"
                  value={this.state.language}
                  disabled={false}
                  onChange={(evt, i, language) => this.setState({ language, jurisdiction: language })}
                >
                  {getLanguageMenuItems()}
                </SelectField>
                <br />
                {this.state.showOptions && advancedOptions}
              </div>
              {this.state.showExamples &&
                <div style={{ flex: 3, marginLeft: 50, maxHeight: 200, overflow: 'auto' }}>
                  <ExampleBlock onExampleSelect={this.loadExample} />
                </div>
              }
            </div>
          </CardText>
          <CardActions>
            <IconButton
              tooltip="Parsing options"
              onTouchTap={() => this.setState(state =>
                                    ({ showOptions: !state.showOptions }))}
            >
              <ActionSettings />
            </IconButton>
            <IconButton
              tooltip="Examples"
              onTouchTap={() => this.setState(state =>
                                    ({ showExamples: !state.showExamples }))}
            >
              <UnarchiveIcon />
            </IconButton>
            <IconButton
              tooltip="Reset all fields"
              onTouchTap={this.setInitialState}
            >
              <RefreshIcon />
            </IconButton>
          </CardActions>
        </Card>
        <FloatingActionButton
          disabled={this.state.text.length < minInputTextLength ||
                    !this.state.language ||
                    !this.state.jurisdiction}
          style={floatingButtonStyle}
          // Using onTouchTap instead of onClick workaround for safari
          onTouchTap={this.sendRequest}
        >
          <ContentSend />
        </FloatingActionButton>
      </div>
    );
  }
}

export default InputBlock;
