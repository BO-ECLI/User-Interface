import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import spacing from 'material-ui/styles/spacing';
import withWidth, { SMALL } from 'material-ui/utils/withWidth';
import injectTapEventPlugin from 'react-tap-event-plugin';

import Header from './Header';
import InputBlock from './InputBlock';
import ResultBlock from './ResultBlock';
import ProposalResultBlock from './ProposalResultBlock';
import ErrorSnackbar from './ErrorSnackbar';
import Footer from './Footer';
import AboutDialog from './AboutDialog';

injectTapEventPlugin();

const desktopGutter = spacing.desktopGutter;

const getStyle = () => ({
  small: {
    padding: desktopGutter,
  },
  large: {
    padding: desktopGutter * 2,
  },
});

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      references: {},
      proposalReferences: [],
      loading: false,
      errorMessage: '',
      aboutDialogOpen: false,
    };
    this.onLoadingChange = this.onLoadingChange.bind(this);
    this.onReferencesChange = this.onReferencesChange.bind(this);
    this.onRequestError = this.onRequestError.bind(this);
    this.onAbout = this.onAbout.bind(this);
  }

  onLoadingChange(loading) {
    this.setState({ loading });
  }

  onReferencesChange(references) {
    // Temporary just until the output format will be defined
    if (Array.isArray(references)) {
      this.setState({ proposalReferences: references, references: {} });
    } else {
      this.setState({ proposalReferences: [], references });
    }
  }

  onRequestError(errorMessage) {
    this.setState({ errorMessage, loading: false });
  }

  onAbout() {
    this.setState({ aboutDialogOpen: true });
  }

  render() {
    const width = this.props.width;
    const style = getStyle();
    return (
      <MuiThemeProvider>
        <div>
          <Header onAbout={this.onAbout} />
          <div style={width === SMALL ? style.small : style.large}>
            <InputBlock
              onLoadingChange={this.onLoadingChange}
              onReferencesChange={this.onReferencesChange}
              onRequestError={this.onRequestError}
            />
            { Object.keys(this.state.references).length === 0 &&
              <ProposalResultBlock
                references={this.state.proposalReferences}
                loading={this.state.loading}
              />
            }
            { Object.keys(this.state.references).length > 0 &&
              <ResultBlock
                data={this.state.references}
                loading={this.state.loading}
              />
            }
            { this.state.errorMessage.length > 0 &&
              <ErrorSnackbar
                message={this.state.errorMessage}
                onRequestClose={() => this.setState({ errorMessage: '' })}
              />
            }
          </div>
          <Footer />
          { this.state.aboutDialogOpen === true &&
            <AboutDialog
              open={this.state.aboutDialogOpen}
              onClose={() => this.setState({ aboutDialogOpen: false })}
            />
          }
        </div>
      </MuiThemeProvider>
    );
  }
}

export default withWidth()(App);
