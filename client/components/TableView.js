import React from 'react';
import { Table, TableBody, TableHeader,
        TableHeaderColumn, TableRow } from 'material-ui/Table';

import ReferenceRow from './ReferenceRow';

const columnStyles = {
  type: {
    width: '15%',
  },
  fields: {
    width: '35%',
  },
};

const featureNames = [
  'context',
  'partition',
  'authority',
  'type',
  'section',
  'subject',
  'day',
  'month',
  'year',
  'doc-year',
  'full-number',
  'number',
  'number-prefix',
  'full-case-number',
  'case-number',
  'case-year',
  'case-prefix',
  'full-alt-number',
  'alt-number',
  'alt-year',
  'alt-prefix',
  'applicant',
  'defendant',
  'geographic',
  'misc',
  'alias',
  'provenance',
];

const TableView = ({ references }) => {
  if (!references.length) return null;
  const createReferenceRow = (ref, extraFields, index) => (
    <ReferenceRow
      key={index}
      columnStyles={columnStyles}
      extraFields={extraFields}
      {... ref}
    />
  );
  const fieldNames = {
    type: 'Document type',
    number: 'Document number',
    date: 'Document date',
  };
  const referenceRows = references.map((ref, index) => {
    let extraFields = [];
    extraFields = featureNames
                    .filter(name => ref.hasOwnProperty(name))
                    .filter(name => name !== 'context' || ref.context !== ref.text)
                    .map(key => ({
                      name: key in fieldNames ? fieldNames[key] : key,
                      value: ref[key],
                    }));
    return createReferenceRow(ref, extraFields, index);
  });

  return (<div>
    <Table>
      <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
        <TableRow>
          <TableHeaderColumn>Legal references</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {referenceRows}
      </TableBody>
    </Table>
  </div>);
};

export default TableView;
