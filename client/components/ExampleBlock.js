import React from 'react';
import { List, ListItem } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
import { grey800, transparent } from 'material-ui/styles/colors';
import FileIcon from 'material-ui/svg-icons/editor/insert-drive-file';

import ITExamples from '../examples/IT.json';
import ESExamples from '../examples/ES.json';

const truncate = (str, length) =>
  str.length > length ? str.substr(0, length - 1) + '...' : str;

const createExampleListItems = (lang, examples, onTouchTap) =>
  examples.map((example, index) => {
    const data = Object.assign(example, {
      language: lang,
    });
    const onTouchFn = onTouchTap.bind(this, data);
    const { inputEcli, text } = example;
    return (
      <ListItem
        key={index}
        primaryText={inputEcli}
        secondaryText={truncate(text, 75)}
        leftAvatar={
          <Avatar color={grey800} backgroundColor={transparent}>
            {lang}
          </Avatar>
        }
        rightIcon={example.fullDocument && <FileIcon />}
        onTouchTap={onTouchFn}
      />
    );
  });

const ExampleBlock = ({ onExampleSelect }) => {
  const ITItems = createExampleListItems('IT', ITExamples, onExampleSelect);
  const ESItems = createExampleListItems('ES', ESExamples, onExampleSelect);
  return (
    <div>
      <List>
        <Subheader>Examples</Subheader>
        {ITItems}
      </List>
      <Divider inset />
      <List>
        {ESItems}
      </List>
    </div>
  );
};

export default ExampleBlock;
