import React from 'react';
import { List, ListItem } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';

const DebugView = ({ logItems }) => {
  const listItems = logItems.map((log, index) =>
    <ListItem
      key={index}
      primaryText={log}
    />
  );

  return (
    <List>
      <Subheader>Parser Engine Log</Subheader>
      {listItems}
    </List>
  );
};

export default DebugView;
