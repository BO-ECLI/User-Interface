import React from 'react';
import Highlight from 'react-highlight';

require('highlight.js/styles/github-gist.css'); // eslint-disable-line

const EcliReferenceMetadataView = ({ references }) => {
  const xmlReferences = references.map(({ code, lang, relation, type }) =>
  `    <reference lang="${lang}" relation="${relation}" type="${type}">${code}</reference>`
  ).join('\n');
  const xml = `
<EcliReferencesMetadata>
${xmlReferences}
</EcliReferencesMetadata>`;
  return (<Highlight className="xml">
    { xml }
  </Highlight>);
};

export default EcliReferenceMetadataView;
