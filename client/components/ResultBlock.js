import React, { Component } from 'react';
import { Card, CardTitle, CardText } from 'material-ui/Card';
import CircularProgress from 'material-ui/CircularProgress';
import { Tabs, Tab } from 'material-ui/Tabs';
import spacing from 'material-ui/styles/spacing';

import JsonView from './JsonView';
import TableView from './TableView';
import HtmlView from './HtmlView';
import DebugView from './DebugView';
import EcliReferenceMetadataView from './EcliReferenceMetadataView';
import json2jsonld from '../../utils/json2jsonld';

const desktopGutter = spacing.desktopGutter;

const cardStyle = {
  marginTop: desktopGutter,
  marginBottom: 100,
};

class ResultBlock extends Component {
  render() {
    const resultData = this.props.data;
    const references = resultData.references;
    const { 'legal-reference': legalReferences } = references;
    const referencesCount = legalReferences ? legalReferences.length : 0;
    const progress = (<CardText style={{ textAlign: 'center' }}>
      <CircularProgress size={80} thickness={5} />
    </CardText>);
    const { 'annotated-text': annotatedText,
            log: logItems,
            'ecli-reference-metadata': ecliReferencesMetadata } = resultData;

    const { reference: referencesMetadata } = ecliReferencesMetadata;
    let jsonLd = [];
    try {
      jsonLd = referencesCount && json2jsonld(resultData);
    } catch (e) {
      console.error('json2jsonld', e);
    }
    const tabs = (
      <Tabs>
        { annotatedText && annotatedText.length > 0 &&
          <Tab label="Annotated text" >
            <HtmlView html={annotatedText} />
          </Tab>
        }
        <Tab label="Table view" >
          <TableView references={legalReferences} />
        </Tab>
        <Tab label="JSON view" >
          { jsonLd && jsonLd.length > 0 ? (
            <Tabs>
              <Tab label="JSON-LD" >
                <JsonView data={jsonLd} />
              </Tab>
              <Tab label="JSON" >
                <JsonView data={resultData} />
              </Tab>
            </Tabs>
          ) : (
            <JsonView data={references} />
          )}
        </Tab>
        { referencesMetadata && referencesMetadata.length > 0 &&
          <Tab label="ECLI Reference metadata" >
            <EcliReferenceMetadataView references={referencesMetadata} />
          </Tab>
        }
        { logItems && logItems.length > 0 &&
          <Tab label="Debug" >
            <DebugView logItems={logItems} />
          </Tab>
        }
      </Tabs>
    );

    return (
      <Card style={cardStyle}>
        <CardTitle
          title="Result"
          subtitle={!this.props.loading ? `References: ${referencesCount}` : ''}
        />
        { this.props.loading && progress }
        {!this.props.loading && referencesCount && (<CardText>{tabs}</CardText>)}
      </Card>
    );
  }
}

export default ResultBlock;
