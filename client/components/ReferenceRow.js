import React from 'react';
import { TableRow, TableRowColumn } from 'material-ui/Table';
import { grey300 } from 'material-ui/styles/colors';
import _ from 'underscore';

import ReferenceItem from './ReferenceItem';

const featureNameStyle = {
  textTransform: 'uppercase',
  display: 'block',
  fontWeight: 'bold',
  fontSize: 10,
};

const featureStyle = {
  whiteSpace: 'normal',
  wordWrap: 'break-word',
  wordBreak: 'break-all',
  backgroundColor: grey300,
  padding: '5px',
  margin: 5,
};

const createFeature = (name, value, index) => {
  const postkey = index || '';
  return (
    <div key={name + postkey} style={featureStyle}>
      <span style={featureNameStyle}>{name}</span>
      {value}
    </div>);
};

const createReferenceItems = references =>
  // Get only the first reference for each type
  _.values(
    _.mapObject(_.groupBy(references, 'type'), refers => refers[0])
  )
  .map((reference, index) =>
    createFeature('legal-identifier',
      <ReferenceItem {...reference} />, index)
  );

const ReferenceRow = (props) => {
  const { text, 'legal-identifier': references,
        'is-case-law-reference': isCaseLaw,
        'is-legal-reference': isLegalReference, columnStyles,
        extraFields } = props;

  const extraRows = extraFields.map(({ name, value }) =>
    createFeature(name, value)
  );
  const type = isLegalReference ? 'Legislative' :
              isCaseLaw ? 'Case Law' : 'Legal';

  return (
    <TableRow>
      <TableRowColumn style={columnStyles.fields}>
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          {createFeature('type', type)}
          {createFeature('text', text)}
          { extraRows.length > 0 &&
            extraRows
          }
          { references && references.length > 0 &&
            createReferenceItems(references)
          }
        </div>
      </TableRowColumn>
    </TableRow>
  );
};

export default ReferenceRow;
