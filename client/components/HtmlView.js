import React from 'react';

const HtmlView = ({ html }) => {
  const newHtml = html.replace(/<a/gi, '<a target="_blank"')
              .replace(/href="([^"]+)"/gi, 'title="$1" href="$1"');
  return (
    <div
      style={{ margin: 14 }}
      dangerouslySetInnerHTML={{ __html: newHtml }}
    />
  );
};

export default HtmlView;
