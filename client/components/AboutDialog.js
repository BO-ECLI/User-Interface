import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

const AboutDialog = ({ onClose }) => {
  const actions = [
    <FlatButton
      label="Ok"
      primary
      onTouchTap={onClose}
    />,
  ];

  return (
    <Dialog
      title="About BO-ECLI parser"
      actions={actions}
      modal={false}
      open
      onRequestClose={onClose}
    >
        <p>The BO-ECLI Parser is the extensible solution for legal link extraction developed
        within the Workstream 2 “Linking Data” of the EU co-funded project “Building On ECLI”</p>

        <p>It addresses the problem of automatic extraction of legal (case-law, legislation) references
        from case-law texts issued in the European context.</p>

        <p>The core of the extraction process is the BO-ECLI Parser Engine and its national extensions
        to languages and jurisdictions of European member states.
        <br />
        Currently implementations have been developed for the Italian and Spanish languages and are
        made accessible in this demo (check the examples, or paste your own text to try).</p>

        <p>This User Interface and its server components allow the interaction of the users with the
        legal link extraction components and integrate the different distributed services thanks to the
        exchange of a structured interoperable JSON  or XML format. </p>

        <p>The Engine and its national extensions are available as Java libraries, can be deployed in
        a web application and are made accessible as a service on the web through an HTTP Rest-Api. </p>

        <p>The BO-ECLI Parser is open source software released with EUPL 1.1 license.
        Check the project code repository for source code and documentation at <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/BO-ECLI">https://gitlab.com/BO-ECLI</a>
        </p>
    </Dialog>
  );
};

export default AboutDialog;
