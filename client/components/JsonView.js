import React from 'react';
import Highlight from 'react-highlight';

require('highlight.js/styles/github-gist.css'); // eslint-disable-line

const JsonView = ({ data }) =>
  (<Highlight className="json">
    { JSON.stringify(data, null, 2) }
  </Highlight>);

export default JsonView;
