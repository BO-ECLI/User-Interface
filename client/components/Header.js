import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import ActionHelp from 'material-ui/svg-icons/action/help';

const BOECLIlogo = require('../img/BO-ECLI-logo.png');

const styles = {
  largeIcon: {
    width: 50,
    height: 50,
  },
  large: {
    width: 50,
    height: 50,
    padding: 0,
  },
};

const Header = ({ onAbout }) =>
  <AppBar
    title="BO-ECLI parser"
    iconElementLeft={
      <IconButton
        iconStyle={styles.largeIcon}
        style={styles.large}
        href="http://www.bo-ecli.eu"
      >
        <img src={BOECLIlogo} alt="BO-ECLI logo" />
      </IconButton>
    }
    iconElementRight={
      <IconButton
        tooltip="Help"
        onTouchTap={onAbout}
      >
        <ActionHelp />
      </IconButton>
    }
  />;

export default Header;
