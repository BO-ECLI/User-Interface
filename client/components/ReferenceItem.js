import React from 'react';

const ReferenceItem = ({ type, code, url }) => (
  <div>{type}: <a style={{ display: 'inline-block' }} title={url} href={url} target="_blank" rel="noopener noreferrer">
    {`${code}`}
  </a></div>
);

export default ReferenceItem;
