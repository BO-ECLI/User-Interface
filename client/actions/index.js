import axios from 'axios';

export function sendParseRequest(data) {
  return axios.post('restApi/run', data);
}
export function sendCompelRequest(data) {
  return axios.post('restApi/compel', data);
}
export function getLanguages() {
  return axios.get('restApi/languages');
}
export function getJurisdictions() {
  return axios.get('restApi/jurisdictions');
}
