/* eslint-disable */

import webpack from 'webpack';
import config from './server/config';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import cssnext from 'postcss-cssnext';
import postcssReporter from 'postcss-reporter';
import { resolve } from 'path';

module.exports = {
  entry: [
    `webpack-hot-middleware/client?http://0.0.0.0:${config.port}/`,
    'webpack/hot/only-dev-server',
    './client/index.js',
  ],
  output: {
    path: __dirname,
    filename: 'app.js',
    publicPath: `http://localhost:${config.port}/`,
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: 'client/index.tpl.html',
      inject: 'body',
      filename: 'index.html',
    }),
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      'client',
      'node_modules',
    ],
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015', 'es2017', 'react'],
        plugins: ['transform-runtime'],
      },
    }, {
      test: /\.jsx?$/,
      include: [resolve(__dirname, 'node_modules/axios')],
      loader: 'babel-loader',
      query: {
        presets: ['es2015', 'es2017', 'react'],
        plugins: ['transform-runtime'],
      },
    }, {
      test: /\.css$/,
      exclude: /node_modules/,
      use: [
        'style-loader',
        'css-loader',
        'postcss-loader'
      ]
    }, {
      test: /\.css$/,
      include: /node_modules/,
      use: ['style-loader', 'css-loader'],
    }, {
      test: /\.(png|jpg|jpeg|gif|woff)$/,
      exclude: /node_modules/,
      use: ['url-loader'],
    }],
  },
};
