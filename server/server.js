import Express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
// Webpack for development
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackDevConfig from '../webpack.config.dev';

import config from './config';
import restApiRouter from './services/RestApi';

const app = new Express();

if (process.env.NODE_ENV === 'development') {
  const compiler = webpack(webpackDevConfig);
  app.use(webpackDevMiddleware(compiler, {
    publicPath: webpackDevConfig.output.publicPath,
  }));
  app.use(webpackHotMiddleware(compiler));
}

// Serving files in production
app.use(Express.static(path.resolve(__dirname, '../dist')));
app.use(bodyParser.json());
app.use('/restApi', restApiRouter);

app.listen(config.port, (error) => {
  if (error) console.error(error);
  console.log(`bo-ecli-ui is running at ${config.port}!`);
});
