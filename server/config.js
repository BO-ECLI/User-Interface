export default {
  port: process.env.PORT || 9000,
  restApiUrl: 'http://dev.ittig.cnr.it:8080/entry-point',
};
