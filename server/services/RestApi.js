// This service is a wrapper to the BO-ECLI Rest-Api
import { Router } from 'express';
import axios from 'axios';

import config from '../config';
import dummyCassazioneData from './dummyCassazioneData.json';
import dummyReferences from './dummyLegalReferences.json';
import Compel from 'compel';

const router = new Router();
const restInst = axios.create({
  baseURL: config.restApiUrl,
});

const onError = (res, error) => {
  if (error.response) {
    res.status(error.response.status).send(error.response.data);
  } else {
    console.log('Error', error.message);
    res.status(500).send(error.message);
  }
};

router.post('/run', (req, res) => {
  if (req.body.text === 'Penale Sent. Sez. 1 Num. 13349 Anno 2013') {
    res.json(dummyCassazioneData.references);
    return;
  }
  if (req.body.text === 'example') {
    res.json(dummyReferences);
    return;
  }
  restInst.post('/run', req.body)
    .then((response) => {
      res.json(response.data);
    })
    .catch(onError.bind(this, res));
});

router.post('/compel', (req, res) => {
  const compel = new Compel();
  async function fillJson() {
    try {
      const jsonFilled = await compel.fillMissingFeatures(req.body.json);
      res.json(jsonFilled);
    } catch (err) {
      onError(res, err);
    }
  }
  fillJson();
});

router.get('/languages', (req, res) => {
  restInst.get('/languages')
    .then((response) => {
      res.json('language' in response.data ? response.data.language : []);
    })
    .catch(onError.bind(this, res));
});

router.get('/jurisdictions', (req, res) => {
  restInst.get('/jurisdictions')
    .then((response) => {
      res.json('jurisdiction' in response.data ? response.data.jurisdiction : []);
    })
    .catch(onError.bind(this, res));
});


export default router;
