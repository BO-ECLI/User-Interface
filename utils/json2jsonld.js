import _ from 'underscore';

const jsonLDTpl = {
  '@context': [
    'http://www.bo-ecli.it/legalcitem/lcm-context7.json',
    'http://www.bo-ecli.it/legalcitem/lcm-local.json',
  ],
  citation: {
    details: {
      text: '',
    },
  },
  references: [],
};

const featureTpl = {
  name: '',
  frame: 'source',
  frbr: 'work',
  levels: [],
};

const featureDrop = ['text', 'context', 'provenance', 'day', 'month', 'year', 'legal-identifier'];

const featureMapping = {
  type: 'document type',
  number: 'document number',
};

const convertFeature = (data) => {
  const [name, value] = data;
  const feature = Object.assign({}, featureTpl);
  feature.name = featureMapping[name] ? featureMapping[name] : name;
  feature.levels = { values: value };
  return feature;
};

const createDateFeature = ({ year, month, day }) => {
  const dateValues = _.compact([year, month, day])
                      .map((value) => {
                        return { values: value.replace(/-/g, '') };
                      });
  if (dateValues.length === 0) return [];
  const feature = Object.assign({}, featureTpl);
  feature.name = 'document date';
  feature.levels = dateValues.length === 1 ? dateValues[0] : dateValues;
  return feature;
};

const createFeature = data =>
  Object.assign(Object.assign({}, featureTpl), data);

const createLegalIdentifierFeature = ({ 'legal-identifier': legalIdentifiers }) => {
  if (!legalIdentifiers || legalIdentifiers.length === 0) return [];
  return legalIdentifiers.map(({ type, code, provenance, confidence }) => {
    const feature = {
      name: (type || 'legal identifier').toLowerCase(),
      author: { fullname: provenance },
      confidence,
      levels: { values: code },
    };
    return createFeature(feature);
  });
};

const createSelectedTextFeature = ({ text, context }) => {
  if (!text || !context || text === context) return [];
  return createFeature({
    name: 'selectedText',
    frame: 'interpretation',
    levels: { values: text },
  });
};

const convertReference = (data) => {
  const jsonLD = Object.assign({}, jsonLDTpl);
  const text = data.context && data.context !== data.text ? data.context : data.text;
  jsonLD.citation.details.text = text;
  jsonLD.references = [{
    features: _.pairs(data)
              .filter(feature => featureDrop.indexOf(feature[0]) < 0)
              .map(convertFeature)
              .concat(createDateFeature(data))
              .concat(createLegalIdentifierFeature(data))
              .concat(createSelectedTextFeature(data)),
  }];
  return jsonLD;
};

const getLegalReferences = data =>
  data.references && data.references['legal-reference'];

const json2jsonld = (jsonData) => {
  const references = getLegalReferences(jsonData);
  const res = references.map(convertReference);
  return res;
};

export default json2jsonld;
