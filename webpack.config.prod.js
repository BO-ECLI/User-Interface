/* eslint-disable */
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var cssnext = require('postcss-cssnext');
var postcssReporter = require('postcss-reporter');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin');
var path = require('path');

module.exports = {
  entry: [
    './client/index.js',
  ],
  output: {
    path: __dirname + '/dist/',
    filename: '[name].[chunkhash].js',
    publicPath: process.env.PUBLIC_PATH || '/',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production'),
      }
    }),
    new CleanWebpackPlugin(['dist'], {
      verbose: true,
      exclude: ['server.bundle.js']
    }),
    new ExtractTextPlugin('[name].[chunkhash].css'),
    new HtmlWebpackPlugin({
      template: 'client/index.tpl.html',
      inject: 'body',
      filename: 'index.html',
    }),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false,
      }
    }),
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      'client',
      'node_modules',
    ],
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015', 'es2017', 'react'],
        plugins: ['transform-runtime'],
      },
    }, {
      test: /\.jsx?$/,
      include: [path.resolve(__dirname, 'node_modules/axios')],
      loader: 'babel-loader',
      query: {
        presets: ['es2015', 'es2017', 'react'],
        plugins: ['transform-runtime'],
      },
    }, {
      test: /\.css$/,
      exclude: /node_modules/,
      loader: ExtractTextPlugin.extract({
        fallbackLoader: 'style-loader',
        loader: [
          {
            loader: 'css-loader',
            options: {
              modules: true
            }
          },
          {
            loader: 'postcss-loader'
          }
        ]
        })
    }, {
      test: /\.css$/,
      include: /node_modules/,
      use: ['style-loader', 'css-loader'],
    }, {
      test: /\.(png|jpg|jpeg|gif|woff)$/,
      exclude: /node_modules/,
      use: ['url-loader'],
    }],
  },
};
