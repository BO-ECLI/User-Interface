import { expect } from 'chai';
import json2jsonld from '../utils/json2jsonld';

import example1 from './resources/example-1.json';
import example1ld from './resources/example-1-ld.json';
import example2 from './resources/example-2.json';
import example2ld from './resources/example-2-ld.json';
import example3 from './resources/example-3.json';
import example3ld from './resources/example-3-ld.json';

describe('json2jsonld', () => {
  it('example1 should be converted to example1ld', () => {
    const converted = json2jsonld(example1);
    expect(JSON.stringify(converted)).to.equal(JSON.stringify(example1ld));
  });
  it('example2 should be converted to example2ld', () => {
    const converted = json2jsonld(example2);
    expect(JSON.stringify(converted)).to.equal(JSON.stringify(example2ld));
  });
  it('example3 should be converted to example3ld', () => {
    const converted = json2jsonld(example3);
    expect(JSON.stringify(converted)).to.equal(JSON.stringify(example3ld));
  });
});
