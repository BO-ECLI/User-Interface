# BO-ECLI Parser User Interface

The BO-ECLI Parser User Interface

## Install and Running

`git clone https://gitlab.com/BO-ECLI/User-Interface.git`

`cd User-Interface`

`npm install`

### Start developement environment
`npm start`

Open [http://localhost:9000/](http://localhost:9000/) in the browser.

## Building and deployment
### Build client
`npm run build`

### Build server
`npm run build:server`

### Deploy
The build process saves all files to the `build` folder, in order to deploy the project you need to upload those files to the server.

## View examples
Write `dummy` in the text box in order to load some dummy references.
Write `Penale Sent. Sez. 1 Num. 13349 Anno 2013` in the text box in order to load an example reference in the proposed JSON format.